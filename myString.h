#include <iostream>
#include <string>
int getLength(string str);
char getCharUsingSquareBrackets(int index);

char getCharUsingRoundBrackets(int index);

char getFirstChar();

string getStr();
char getLastChar();

bool isEqual(string s);

int getCharFirstOccurrenceInd(char c);
int gstStringFirstOccurrenceInd(string str);

int getCharLastOccurrenceInd(char c);
